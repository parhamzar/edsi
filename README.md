Parham Zar, is the Founder and Managing Director of the Egg Donor & Surrogacy Institute (EDSI) and a leading expert within the third party assisted reproductive industry. Over his 20 year tenure at EDSI, Parham has helped thousands of intended parents bring their dream of parenthood to fruition. He is deeply invested in creating a more compassionate and open dialogue around infertility and reproductive challenges; issues that commonly impact individuals and couples around the world.

Website: http://parhamzar.com
